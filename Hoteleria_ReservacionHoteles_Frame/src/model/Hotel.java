/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.ArrayList;

/**
 *
 * @author reyes
 */
public class Hotel {
    private Habitacion lista[];

    public Hotel() {
        lista = new Habitacion[20];

        for (int i = 0; i <= 5; i++) 
            lista[i] = new Habitacion(i + 1, TipoHabitacion.EJECUTIVA, 1);        

        for (int i = 6; i <= 9; i++) 
            lista[i] = new Habitacion(i + 1, TipoHabitacion.DOBLE, 2);        

        for (int i = 10; i <= 18; i++) 
            lista[i] = new Habitacion(i + 1, TipoHabitacion.SUITE, 5);        

        lista[19] = new Habitacion(20, TipoHabitacion.PRESIDENCIAL, 10);
    }

    public int checkIn(String nombre, int ced, int edad, TipoHabitacion tipoHabitacion, ArrayList<Usuario> huespedes) {
        int habitacion=0;
        for (int i = 0; i < lista.length; i++) {
            if (lista[i].getCategoria().equals(tipoHabitacion) && lista[i].isDisponible() ) {                
                lista[i].setResponsable(new Responsable(nombre, ced, edad));
                lista[i].setListaUsuarios(huespedes);
                habitacion = i;                    
                i = lista.length;                
            }
        }
        return habitacion;
    }
    
    public boolean isHuespedHotel(int cedula){
        boolean esta = false;
        for (int i = 0; i < lista.length; i++) {
            if(lista[i].isUsuario(cedula));{
                esta = true;
                i = lista.length;
            }
        }        
        return esta;
    }
    
    public void registrarUsuario(int idHabitacion, String nombre, int ced, int edad){
        Habitacion habitacion = lista[idHabitacion-1];
        
        if(habitacion.getListaUsuarios().size() != 0){
            if(habitacion.getListaUsuarios().size() < habitacion.getCapacidad())
                habitacion.getListaUsuarios().add(new Usuario(nombre, ced, edad));
            else
                System.out.println("No se puede registrar, supera capacidad máxima de huespédes: <"+habitacion.getCapacidad()+">");
        }
        else
            System.out.println("Se debe hacer checkIn para registrar el Responsable");
    }
    
    public void darSalida(int id) {
        Habitacion habitacion = lista[id-1];
        lista[id-1] = null;
        lista[id-1] = new Habitacion(id, habitacion.getCategoria(), habitacion.getCapacidad());
    }
  
    public int habitacionMayorDeuda() {
        int hab = 0;
        float mayor = lista[0].getPrecio();
        
        for (Habitacion habitacion : lista) {
            if(habitacion.getPrecio() > mayor){
                mayor = habitacion.getPrecio();
                hab = habitacion.getId();
            }
        }        
        return hab;
    }
    
    public int buscarHabitacionCed(int cedula) {
        int habitacion = 0;
        for (int i = 0; i < lista.length; i++) {
            for (int j = 0; j < lista[i].getListaUsuarios().size(); j++) {
            
      
            }            
        }
        return habitacion;
    }

    public int habitacionesDisponibles() {
        int count = 0;
        for (Habitacion habitacion : lista) {
            if (habitacion.isDisponible()) {
                count++;
            }
        }
        return count;
    }

    public int habitacionesOcupadas() {        
        return 20-habitacionesDisponibles();
    }

    public Habitacion[] getLista() {
        return lista;
    }

    public void setLista(Habitacion[] lista) {
        this.lista = lista;
    }

    private void size() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
