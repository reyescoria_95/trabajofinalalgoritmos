/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author reyes
 */
public class Reserva {
    private int id;
    private Usuario usuario;
    private String fechaIngreso;
    private String fechaSalida;
    private int nroHabitaciones;
    private TipoHabitacion tipohabitacion;

    public Reserva(int id, Usuario usuario, String fechaIngreso, String fechaSalida, int nroHabitaciones, TipoHabitacion tipohabitacion) {
        this.id = id;
        this.usuario = usuario;
        this.fechaIngreso = fechaIngreso;
        this.fechaSalida = fechaSalida;
        this.nroHabitaciones = nroHabitaciones;
        this.tipohabitacion = tipohabitacion;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public String getFechaIngreso() {
        return fechaIngreso;
    }

    public void setFechaIngreso(String fechaIngreso) {
        this.fechaIngreso = fechaIngreso;
    }

    public String getFechaSalida() {
        return fechaSalida;
    }

    public void setFechaSalida(String fechaSalida) {
        this.fechaSalida = fechaSalida;
    }

    public int getNroHabitaciones() {
        return nroHabitaciones;
    }

    public void setNroHabitaciones(int nroHabitaciones) {
        this.nroHabitaciones = nroHabitaciones;
    }

    public TipoHabitacion getTipohabitacion() {
        return tipohabitacion;
    }

    public void setTipohabitacion(TipoHabitacion tipohabitacion) {
        this.tipohabitacion = tipohabitacion;
    }
    
    
}
