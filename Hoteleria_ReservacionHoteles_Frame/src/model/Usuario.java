/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author reyes
 */
public class Usuario extends Persona {
    public Usuario() {
        super(null, 0, 0);
    }

    public Usuario(String nombre, int cedula, int edad) {
        super(nombre, cedula, edad);
    }
}
