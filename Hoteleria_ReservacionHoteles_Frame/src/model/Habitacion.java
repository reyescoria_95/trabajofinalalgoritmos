/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.ArrayList;

/**
 *
 * @author reyes
 */
public class Habitacion implements Comparable{
    
    private int id;
    private int capacidad;
    private TipoHabitacion categoria;
    private ArrayList<Usuario> listaUsuarios;
    private float precio;
    private boolean disponible;
    int getListausuarios;
    
    public Habitacion(int id, TipoHabitacion categoria, int capacidad)
    {
        listaUsuarios = new ArrayList();
        this.id = id;
        this.categoria = categoria;
        this.capacidad = capacidad;
    }
    
    public boolean isDisponible()
    {
        return disponible;
    }
    
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCapacidad() {
        return capacidad;
    }

    public void setCapacidad(int capacidad) {
        this.capacidad = capacidad;
    }

    public TipoHabitacion getCategoria() {
        return categoria;
    }

    public void setCategoria(TipoHabitacion categoria) {
        this.categoria = categoria;
    }

    public float getPrecio() {
        return precio;
    }

    public void setPrecio(float precio) {
        this.precio = precio;
    }

    public ArrayList<Usuario> getListaUsuarios() {
        return listaUsuarios;
    }

    public void setListaUsuarios(ArrayList<Usuario> listaUsuarios) {
        this.listaUsuarios = listaUsuarios;
    }
    
/*incompleto*/

    @Override
    public int compareTo(Object o) {
        Habitacion hab = (Habitacion) o;
        return 0;
    }

    boolean isUsuario(int cedula) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    void setResponsable(Responsable responsable) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }


}
